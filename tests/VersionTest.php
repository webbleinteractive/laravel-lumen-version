<?php
namespace Webble\Version\Test;

use Webble\Version\Version;

class VersionTest extends TestCase
{
    /** @test */
    function it_returns_the_default_app_version()
    {
        $package = new Version();

        $this->assertEquals("1.0.0", $package->current());
    }

    /**
     * @test
     */
    function it_bumps_major_part_of_version()
    {
        $package = new Version();

        $this->assertEquals("2.0.1", $package->bumpMajor("1.0.1"));
    }

    /**
     * @test
     */
    function it_bumps_minor_part_of_version()
    {
        $package = new Version();

        $this->assertEquals("2.7.1", $package->bumpMinor("2.6.1"));
    }

    /**
     * @test
     */
    function it_bumps_patch_part_of_version()
    {
        $package = new Version();

        $this->assertEquals("2.5.27", $package->bumpPatch("2.5.26"));
    }
}
