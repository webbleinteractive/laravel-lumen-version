<?php


namespace Webble\Version\Console\Commands;


use Illuminate\Console\Command;
use Webble\Version\Version;

class VersionCommand extends Command
{
    protected $signature = 'version:get';

    protected $description = 'Get your app\'s current version.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Version $version)
    {
        $this->line($version->current());
    }
}
