<?php


namespace Webble\Version;


class Version
{

    function current()
    {
        return config('version.current');
    }

    function bumpMajor(string $version)
    {
        // break string
        $parts = explode(".", $version);

        // add 1
        $major = (intval($parts[0]) + 1);

        // join string
        $parts[0] = strval($major);

        return implode(".", $parts);
    }

    function bumpMinor($version)
    {
        // break string
        $parts = explode(".", $version);

        // add 1
        $major = (intval($parts[1]) + 1);

        // join string
        $parts[1] = strval($major);

        return implode(".", $parts);
    }

    function bumpPatch($version)
    {
        // break string
        $parts = explode(".", $version);

        // add 1
        $major = (intval($parts[2]) + 1);

        // join string
        $parts[2] = strval($major);

        return implode(".", $parts);
    }
}
