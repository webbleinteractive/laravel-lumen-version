<?php


namespace Webble\Version\Test;


use Illuminate\Support\Facades\Artisan;

class VersionCommandTest extends TestCase
{
    /**
     * @test
     */
    public function it_returns_current_version()
    {
        Artisan::call('version:get');

        $this->assertEquals("1.0.0\n", Artisan::output());
    }


}
