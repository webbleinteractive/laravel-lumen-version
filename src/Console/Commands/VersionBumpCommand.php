<?php


namespace Webble\Version\Console\Commands;


use Illuminate\Console\Command;
use sixlive\DotenvEditor\DotenvEditor;
use Webble\Version\Version;

class VersionBumpCommand extends Command
{
    protected $signature = 'version:bump {--major : Increment the major part of APP_VERSION} {--minor : Increment the minor part of APP_VERSION} {--patch : Increment the patch part of APP_VERSION}';

    protected $description = 'Bump up your app version by 1.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Version $version)
    {
        // read input
        $isMajor = $this->option('major');
        $isMinor = $this->option('minor');
        $isPatch = $this->option('patch');

        // read current version
        $current = $version->current();
        $newVersion = $current;

        // bump it
        if($isMajor) {
            $this->info('Bumping major version version from: ' . $newVersion);
            $newVersion = $version->bumpMajor($newVersion);
        }

        if($isMinor) {
            $this->info('Bumping minor version version from: ' . $newVersion);
            $newVersion = $version->bumpMinor($newVersion);
        }

        if($isPatch || (!$isPatch && !$isMinor && !$isMajor)) {
            $this->info('Bumping patch version version from: ' . $newVersion);
            $newVersion = $version->bumpPatch($newVersion);
        }

        // persist it
        $this->persistVersion($newVersion);

        $this->line($newVersion);
    }

    protected function persistVersion($version)
    {
        // Lumen Error:
        //  Call to undefined method Laravel\Lumen\Application::environmentPath()
        $app = $this->getLaravel();

        $editor = new DotenvEditor;

        if(method_exists($app,'environmentPath'))
        {
            $envPath = $app->environmentPath();
            $envFile = $app->environmentFile();

            $this->info('Persisting version to: ' . $envPath . '/' . $envFile);
            $editor->load($envPath . '/' . $envFile);
        }
        else
        {
            $this->info('Persisting version to: .env');
            $editor->load(base_path('.env'));
        }

        $editor->set('APP_VERSION', $version);
        $editor->save();
    }
}
