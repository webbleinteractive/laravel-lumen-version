<?php


namespace Webble\Version\Test;


use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Illuminate\Support\Facades\Artisan;
use sixlive\DotenvEditor\DotenvEditor;

class VersionBumpCommandTest extends TestCase
{
    protected $envFile;

    protected function getEnvironmentSetUp($app)
    {
        // make sure our testing .env file is loaded
        $app->useEnvironmentPath(__DIR__.'/..');
        $app->loadEnvironmentFrom('testing.env');

        $this->envFile =$app->environmentPath() . '/' . $app->environmentFile();
        // reset the APP_VERSION
        $editor = new DotenvEditor;

        $editor->load($this->envFile);
        $editor->set('APP_VERSION', "1.0.0");
        $editor->save();

        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::getEnvironmentSetUp($app);
    }

    /**
     * @test
     */
    public function it_increases_version_patch_part()
    {
        // php artisan version:bump --patch
        Artisan::call('version:bump', ['--patch' => true]);

        $this->assertContains("1.0.1", Artisan::output());
    }

    /**
     * @test
     */
    public function it_increases_version_minor_part()
    {
        // php artisan version:bump --minor
        Artisan::call('version:bump', ['--minor' => true]);

        $this->assertContains("1.1.0", Artisan::output());
    }

    /**
     * @test
     */
    public function it_increases_version_major_part()
    {
        // php artisan version:bump --major
        Artisan::call('version:bump', ['--major' => true]);

        $this->assertContains("2.0.0", Artisan::output());
    }

    /**
     * @test
     */
    public function it_increases_version_patch_by_default()
    {
        // php artisan version:bump
        Artisan::call('version:bump');

        $this->assertContains("1.0.1", Artisan::output());
    }

}
