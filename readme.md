# Webble Version Package  

[![Codeship Status for webbleinteractive/laravel-lumen-version](https://app.codeship.com/projects/a5b9d1e0-3ad9-0137-e892-06c3d1201fe1/status?branch=master)](https://app.codeship.com/projects/334259)

Simple version bump for Laravel and Lumen apps.


## Requirements  

* PHP >= 7.2
* Laravel|Lumen >= 5.7


## Installation

```sh
composer require webble/laravel-lumen-version
```

Laravel should auto discover this package. 

Lumen apps will need to register the ServiceProvider  

```php
// modify your bootstrap/app.php file

$app->register(Webble\Version\VersionServiceProvider::class);
```

## Usage

Works with a config/env variable named `APP_VERSION`

### Read Current Version  

Get the current version. This will default to env `APP_VERSION`. If not found the value defined in `config('version.current')` is used.

```sh
php artisan version:get 
```

### Bump Version  

To increment (or bump up) the current version you can use the Artisan command `version:bump`. This will read your application's .env file to fetch the current version, then update it with the new version.

**bump up version patch part**
```sh
# by default it will bump the patch if no flags are provided
php artisan version:bump
 
# specifying --patch also works
php artisan version:bump --patch
```

**bump up version minor part**
```sh
php artisan version:bump --minor
```

**bump up version major part**
```sh
php artisan version:bump --major
```


## Credits  

[Sherri Flemings](https://bitbucket.org/webbleinc/)
