<?php


namespace Webble\Version;


use Illuminate\Support\ServiceProvider;
use Webble\Version\Console\Commands\VersionBumpCommand;
use Webble\Version\Console\Commands\VersionCommand;


class VersionServiceProvider  extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/version.php' => config_path('version.php'),
        ]);

        if($this->app->runningInConsole())
        {
            $this->commands([
                VersionCommand::class,
                VersionBumpCommand::class
            ]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/version.php', 'version'
        );

        $this->app->bind('Version', function($app){
            return new Version();
        });
    }
}
